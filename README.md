![alt text](https://gitlab.com/imadevresponsive/imadevresponsive/-/blob/main/images/GitHubHeader.png 'Banner')

#### Hi!😊 My name is Lianel, and i'm a FullStack Web & Mobile Developer formed in [HENRY](https://www.soyhenry.com/) Academy.

## PERSONAL SKILLS

My self-sacrificing, disciplined and responsible aptitudes were the main keys to walk through this difficult path of make myself a Developer. Also this tragic pandemic was in my case an opportunity to make true that dream of become a Professional of IT Sciences. I love teamwork and leadership, I like to improve and always give my best. Kind, empathetic and enthusiastic character. Passionate by foreign languages and solve technical problems.
Experience in FullStack development of web and mobile applications. Also a specialist in Physical Culture, Sports and Massage therapy. Empirical skills, sales experience and much more when meeting me 😉.

## CURRICULUM ![](https://enqh38om8k81x14.m.pipedream.net)

Here you are an extract of my resume, designed with some of my JavaScript knowledge.

```javascript
/* ============================= myJavaScriptResume ============================= */
import * as technologies from 'IT_World';
import { knowhow } from 'My_Previus_Studies';

const myName = 'Lianel';
const myLastName = 'Artiles Sotolongo';
const myBirthday = new Date(1985, 8 - 1, 8);
const henry = {
    languages: { programLanguages: [], markupLanguages: [] },
    frontendKnowledge: [],
    backendKnowledge: []
};
let myBrainBlowUpWithHenry = henry;
const HenryBootCampReducer = (state = henry, technologies) => {
    switch (technologies.type) {
        case technologies.FRONTEND:
            return {
                ...state,
                frontendKnowledge: [
                    ...henry.frontendKnowledge,
                    technologies.FRONTEND
                ]
            };
        case technologies.BACKEND:
            return {
                ...state,
                backendKnowledge: [
                    ...henry.backendKnowledge,
                    technologies.BACKEND
                ]
            };
        default:
            return state;
    }
};
console.log(HenryBootCampReducer(henry, technologies));
/*
  {
    languages: {
      programLanguages: [ "JavaScript" ],
      markupLanguages: [ "HTML", "CSS", "JSX" ],
    },
    frontendKnowledge: [ "LESS", "React Js", "React-Redux", "React Native", "Expo" ],
    backendKnowledge: [ "Node Js", "Express", "SQL", "Sequelize", "Firebase", "SQLite", "Postgres" ]
  };
*/
!myBrainBlowUpWithHenry
    ? 'My future would not be that promising :('
    : "I'm gonna rock the world (❁´◡`❁)";

const tlbm = 'technologies learned by myself';

export const imadev = {
    myName,
    myLastName,
    age: new Date().getFullYear() - myBirthday.getFullYear(),
    presentation: function () {
        return `My name is ${this.myName} ${this.myLastName} and I am ${this.age} Years Old`;
    },
    previusExperince: knowhow,
    ...henry,
    jobExperience: [{ company: 'REVAI', position: 'FullStack Developer' }]
};

imadev[tlbm] = [
    'MongoDB',
    'MySQL',
    'Styled-Components',
    'Figma',
    'TypeScript',
    'Next.Js',
    'GraphQL'
];
```

### TECHNOLOGIES

| **FullStack** | ✔          | ✔          | ✔            | ✔        | ✔           | ✔             | ✔        | ✔                 | ✔           | ✔       | ✔            | ✔     |
| ------------- | ---------- | ---------- | ------------ | -------- | ----------- | ------------- | -------- | ----------------- | ----------- | ------- | ------------ | ----- |
| **Languages** | JavaScript | TypeScript |              |          |             |               |          |                   |             |         |              |       |
| **Frontend**  | HTML       | CSS        | React Native | React Js | React-Redux | React-Routing | Next.Js  | Styled-Components | Material UI | Figma   | Tailwind CSS | EJS   |
| **Backend**   | NodeJs     | Express    | SQL          | Mongo    | Sequelize   | SQLite        | Postgres | Mongoose          | Firebase    | GraphQL | Contentful   | MySQL |

## MAIN PROJECTS

Here you are some of my projects:

### FRONTEND PROJECTS

✔ [Porfolio:](https://larts85.github.io/lianelartiles/#/): (Hosted with GH Pages) [Repository](https://github.com/larts85/lianelartiles)

✔ [Argenclima](https://argenclima2020.web.app/): (Hosted with Firebase)(React, Styled-Components) (External APIs consumed: _Openweather_, _Wikipedia_) [Repository](https://github.com/larts85/argenclima)

### BACKEND PROJECTS

✔ [LA-Motors](https://github.com/larts85/LA-Motors): (Node Js) (Job application exercise)

✔ [FreeCodeCamp](https://github.com/larts85/Basic-Node-and-Express-FCC): (Express Js)

### FULLSTACK PROJECTS

✔ [Moonbank](https://github.com/larts85/Wallet-Native): (Mobile Team Project)

✔ [Fast-Shopping-Cart](https://larts85.github.io/fast-shopping-client/#/): (Successful interview exercise) [Client repository](https://github.com/larts85/fast-shopping-client) [API repository](https://github.com/larts85/fast-shopping-api)

✔ [Fitness Shop](http://github.com/larts85/Fitness-Shop): (Team Project refactored by me)

## FullStack Developer Certificate

![alt text](https://gitlab.com/imadevresponsive/imadevresponsive/-/blob/main/images/Certificate.png 'Henry Certificate')

## to be continue...

#### Contact me:

[![Gmail Badge](https://img.shields.io/badge/-larts85@gmail.com-c14438?style=flat&logo=Gmail&logoColor=white)](mailto:larts85@gmail.com 'E-mail me')
[![Linkedin Badge](https://img.shields.io/badge/-Lianel%20Artiles-0072b1?style=flat&logo=Linkedin&logoColor=white)](https://www.linkedin.com/in/imadev 'Lets Connect on LinkedIn')
[![Twitter Badge](https://img.shields.io/badge/-@imadev2020-00acee?style=flat&logo=Twitter&logoColor=white)](https://twitter.com/imadev2020 'Follow me on Twitter')
[![Github Badge](https://img.shields.io/badge/-@larts85-666666?style=flat&logo=Github&logoColor=white)](https://github.com/larts85 'Follow me on Github 😉')
